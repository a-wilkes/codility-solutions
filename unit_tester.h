#include <algorithm>
#include <functional>
#include <sstream>
#include <vector>



template<typename T, typename... Args>
class Unit_Tester
{
public:
    auto add_test(std::function<T(Args...)> function, const T expected, const Args... args) -> void;
    auto run_tests() -> void;
    auto test_results() -> std::string;

private:
    struct Test
    {
        Test(const std::function<T(Args...)> function, const T expected, const Args... args)
            : m_function{ std::bind(function, args...) }, m_expected{ expected }
        {}; 

        const std::function<T()> m_function;
        const T m_expected;
    };

    std::vector<Test> m_tests;
    std::ostringstream m_test_results;

    auto run_test(const Test& t, const unsigned int test_number) -> bool;
    auto report_test(const bool status, const T expected, const T actual, const unsigned int test_number) -> void;
    auto report_success() -> void;
    auto report_failure(const T expected, const T actual) -> void;

    template<typename V>
    auto report_value(const V value) -> void;

    template<template <typename...> class Container, typename... Types>
    auto report_value(const Container<Types...> value) -> void;
};



template<typename T, typename... Args>
auto Unit_Tester<T, Args...>::add_test(std::function<T(Args...)> function, const T expected, const Args... args) -> void
{
    m_tests.push_back(Test(function, expected, args...));
}


template<typename T, typename... Args>
auto Unit_Tester<T, Args...>::run_tests() -> void
{
    unsigned int test_number = 0;
    for (const auto& test : m_tests)
    {
        if (!run_test(test, test_number))
        {
            m_test_results << "Aborting remaining tests.\n";
            return;
        }

        test_number += 1;
    }

    m_test_results << "All tests passed.\n";
}


template<typename T, typename... Args>
auto Unit_Tester<T, Args...>::test_results() -> std::string
{
    return m_test_results.str();
}


template<typename T, typename... Args>
auto Unit_Tester<T, Args...>::run_test(const Test& t, const unsigned int test_number) -> bool
{        
    const auto t_result = t.m_function();

    const auto test_status = (t_result == t.m_expected);
    report_test(test_status, t.m_expected, t_result, test_number);

    return test_status;
}


template<typename T, typename... Args>
auto Unit_Tester<T, Args...>::report_test(const bool status, const T expected, const T actual, const unsigned int test_number) -> void
{
    m_test_results << "Test " << test_number;
    status ? report_success() : report_failure(expected, actual);
}


template<typename T, typename... Args>
auto Unit_Tester<T, Args...>::report_success() -> void
{
    m_test_results << " succeeded.\n";
}


template<typename T, typename... Args>
auto Unit_Tester<T, Args...>::report_failure(const T expected, const T actual) -> void
{
    m_test_results << " failed: [actual] ";
    report_value(actual);
    m_test_results << " != ";
    report_value(expected);
    m_test_results << " [expected]\n";
}


template<typename T, typename... Args>
template<typename V>
auto Unit_Tester<T, Args...>::report_value(const V value) -> void
{
    m_test_results << value;
}


template<typename T, typename... Args>
template<template <typename...> class Container, typename... Types>
auto Unit_Tester<T, Args...>::report_value(const Container<Types...> value) -> void
{
    m_test_results << "{ ";
    for (const auto& v : value) { m_test_results << v << ", "; }
    m_test_results << "}";
}
