#include <cstdint>
#include <numeric>
#include <vector>



std::uint_least64_t size_based_summation(const std::uint_least64_t n)
{
    return (n * (n + 1)) / 2;
}


int find_missing_element(const std::vector<int>& input)
{
    using sum_t = std::uint_least64_t;

    const sum_t actual_sum   = std::accumulate(begin(input), end(input), 0);
    const sum_t expected_sum = size_based_summation(input.size());
    const sum_t difference   = actual_sum - expected_sum;

    return (input.size() + 1 - difference);
}


int solution(std::vector<int> &A)
{
    return find_missing_element(A);
}



// main and required includes separated from the above to make copying into Codility cleaner
#include "unit_tester.h"
#include <algorithm>
#include <iostream>

int main()
{
    auto tester = Unit_Tester<int, std::vector<int>>();

    tester.add_test(find_missing_element, 1, { });
    tester.add_test(find_missing_element, 1, { 2 });
    tester.add_test(find_missing_element, 4, { 2, 3, 1, 5 });
    tester.add_test(find_missing_element, 2, { 1, 3, 4 });
    tester.add_test(find_missing_element, 2, { 1, 3, 4, 5 });
    tester.add_test(find_missing_element, 3, { 1, 2, 4, 5, 6 });
    tester.add_test(find_missing_element, 5, { 1, 2, 3, 4, 6, 7 });

    // Start: Generate large test
    const int test_input_size = 100000;
    std::vector<int> test_input(test_input_size);
    std::generate(begin(test_input), end(test_input), [n = 0]() mutable { return ++n; });
    
    const int answer = 57894;
    test_input.at(answer - 1) = test_input_size + 1;

    tester.add_test(find_missing_element, answer, test_input);
    // Stop: Generate large test
    

    tester.run_tests();

    std::cout << tester.test_results() << std::endl;

    return 0;
}
