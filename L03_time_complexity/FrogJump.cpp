#include <cmath>



int determine_jumps_needed(const int start_position, const int end_target, const int jump_distance)
{
    return std::ceil((end_target - start_position) / static_cast<double>(jump_distance));
}


int solution(int X, int Y, int D)
{
    return determine_jumps_needed(X, Y, D);
}



// main and required includes separated from the above to make copying into Codility cleaner
#include "unit_tester.h"
#include <iostream>

int main()
{
    auto tester = Unit_Tester<int, int, int, int>();

    tester.add_test(determine_jumps_needed, 3, 10, 85, 30);
    tester.add_test(determine_jumps_needed, 0, 1,  1,  1);
    tester.add_test(determine_jumps_needed, 0, 1,  1,  100);
    tester.add_test(determine_jumps_needed, 1, 1,  2,  100);
    tester.add_test(determine_jumps_needed, 9, 1,  10, 1);
    tester.add_test(determine_jumps_needed, 5, 65, 79, 3);
    tester.add_test(determine_jumps_needed, 5, 65, 80, 3);
    tester.add_test(determine_jumps_needed, 6, 65, 81, 3);
    tester.add_test(determine_jumps_needed, 1, 1,  1000000000, 1000000000);
    tester.add_test(determine_jumps_needed, 999999999, 1,  1000000000, 1);
    tester.add_test(determine_jumps_needed, 0, 1000000000, 1000000000, 1000000000);

    tester.run_tests();

    std::cout << tester.test_results() << std::endl;

    return 0;
}