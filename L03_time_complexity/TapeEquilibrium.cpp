#include <algorithm>
#include <cmath>
#include <vector>


std::vector<int> generate_partial_sums(const std::vector<int>& input)
{
    std::vector<int> partial_sums(input.size(), input.at(0));

    std::transform(begin(input) + 1, end(input), begin(partial_sums), begin(partial_sums) + 1,
        [](const int value, const int prior_partial_sum) { return prior_partial_sum + value; }
    );

    return partial_sums;
}


std::vector<int> generate_differences(const std::vector<int>& input)
{
    std::vector<int> differences(input.size());

    const auto partial_sums = generate_partial_sums(input);
    const auto total_sum    = partial_sums.at(partial_sums.size() - 1);

    std::transform(begin(partial_sums), end(partial_sums), begin(differences),
        [&total_sum](const int sum)
        {
            const auto remainder_sum = total_sum - sum;
            return std::abs(remainder_sum - sum);
        }
    );

    return differences;
}


int equilibrium(const std::vector<int> &input) {
    const auto differences = generate_differences(input);

    return *std::min_element(begin(differences), end(differences) - 1);
}


int solution(std::vector<int> &A)
{
    return equilibrium(A);
}


// main and required includes separated from the above to make copying into Codility cleaner
#include "unit_tester.h"
#include <iostream>

int main()
{
    auto tester = Unit_Tester<int, std::vector<int>>();

    tester.add_test(equilibrium, 0, { 0, 0, 0 });
    tester.add_test(equilibrium, 0, { -1, -4, -5, -10 });
    tester.add_test(equilibrium, 2, { 0, -1, -4, 10, 3 });
    tester.add_test(equilibrium, 2000, { -1000, 1000 });

    tester.run_tests();

    std::cout << tester.test_results() << std::endl;

    return 0;
}