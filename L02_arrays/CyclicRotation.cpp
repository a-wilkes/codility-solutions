#include <algorithm>
#include <vector>


std::vector<int> cyclic_rotation(const std::vector<int>& input, const int offset)
{
    if (input.size() == 0) { return input; }

    const int actual_offset = input.size() - (offset % input.size());
    
    std::vector<int> rotated(input);
    std::rotate(begin(rotated), begin(rotated) + actual_offset, end(rotated));
    
    return rotated;
}


std::vector<int> solution(std::vector<int> &A, int K)
{
    return cyclic_rotation(A, K);
}


// main and required includes separated from the above to make copying into Codility cleaner
#include "unit_tester.h"
#include <iostream>


int main()
{
    auto tester = Unit_Tester<std::vector<int>, std::vector<int>, int>();

    tester.add_test(cyclic_rotation, {}, {}, 2);
    tester.add_test(cyclic_rotation, { 0, 1, 2 },       { 0, 1, 2 },       0);
    tester.add_test(cyclic_rotation, { 6, 3, 8, 9, 7 }, { 3, 8, 9, 7, 6 }, 1);
    tester.add_test(cyclic_rotation, { 9, 7, 6, 3, 8 }, { 3, 8, 9, 7, 6 }, 3);
    tester.add_test(cyclic_rotation, { 1, 2, 3, 4 },    { 1, 2, 3, 4 },    4);
    tester.add_test(cyclic_rotation, { 4, 5, 7, 0, 1 }, { 1, 4, 5, 7, 0 }, 9);
    tester.add_test(cyclic_rotation, { 3, 4, 5, 1, 2 }, { 1, 2, 3, 4, 5 }, 8);

    tester.run_tests();

    std::cout << tester.test_results() << std::endl;

    return 0;
}