#include <unordered_set>
#include <vector>


int find_unique_value(const std::vector<int>& input)
{
    std::unordered_set<int> values;

    for (const int i : input)
    {
        const auto res = values.emplace(i);
        if (!res.second) { values.erase(res.first); }
    }

    return *begin(values);
}


int solution(std::vector<int> &A)
{
    return find_unique_value(A);
}



// main and required includes separated from the above to make copying into Codility cleaner
#include "unit_tester.h"
#include <iostream>


int main()
{
    constexpr int max_value = 1000000000;

    auto tester = Unit_Tester<int, std::vector<int>>();

    tester.add_test(find_unique_value, 7, {9, 3, 9, 3, 9, 7, 9});
    tester.add_test(find_unique_value, 1, {1});
    tester.add_test(find_unique_value, 1, {1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3});
    tester.add_test(find_unique_value, 1, {max_value, max_value, 1});
    tester.add_test(find_unique_value, max_value, {1, max_value, 1});

    tester.run_tests();

    std::cout << tester.test_results() << std::endl;

    return 0;
}