#include <algorithm>



int count_div(const int start, const int end, const int divisor)
{
    const int first_divisible = std::max(divisor, start + (start % divisor));

    if (first_divisible > end) { return (start == 0) ? 1 : 0; }

    const int size = end - first_divisible;
    const int n = (size / divisor) + 1;

    return (start == 0) ? n + 1 : n;
}


int solution(int A, int B, int K)
{
    return count_div(A, B, K);
}



// main and required includes separated from the above to make copying into Codility cleaner
#include "unit_tester.h"
#include <iostream>

int main()
{
    auto tester = Unit_Tester<int, int, int, int>();

    tester.add_test(count_div, 3,  6, 11, 2);
    tester.add_test(count_div, 3, 11, 20, 3);
    tester.add_test(count_div, 0,  2,  3, 4);
    tester.add_test(count_div, 0,  2,  2, 3);
    tester.add_test(count_div, 1,  2,  2, 2);
    tester.add_test(count_div, 1,  2,  2, 2);
    tester.add_test(count_div, 0,  2,  5, 6);
    tester.add_test(count_div, 1,  2,  5, 4);
    tester.add_test(count_div, 1,  0,  0, 11);
    tester.add_test(count_div, 4,  0,  3, 1);

    tester.add_test(count_div, 2000000001, 0, 2000000000, 1);

    tester.run_tests();

    std::cout << tester.test_results() << std::endl;

    return 0;
}
