#include <algorithm>
#include <vector>



std::vector<int> max_counters(const int counters_size, const std::vector<int>& operations)
{
    std::vector<int> counters (counters_size, 0);

    int max = 0;
    int new_base = 0;

    for (const auto& op : operations)
    {
        const int i = op - 1;

        if (i >= counters_size) { new_base = max; }
        else
        {
            if (counters.at(i) < new_base) { counters.at(i) =  new_base; }
            
            counters.at(i) += 1;

            max = std::max(max, counters.at(i));
        }
    }

    std::transform(begin(counters), end(counters), begin(counters),
        [new_base](const int c) { return (c < new_base) ? new_base : c; }
    );

    return counters;
}


std::vector<int> solution(int N, std::vector<int> &A)
{
    return max_counters(N, A);
}


// main and required includes separated from the above to make copying into Codility cleaner
#include "unit_tester.h"
#include <iostream>

int main()
{
    auto tester = Unit_Tester<std::vector<int>, int, std::vector<int>>();

    tester.add_test(max_counters, { 3, 2, 2, 4, 2 }, 5, { 3, 4, 4, 6, 1, 4, 4 });
    tester.add_test(max_counters, { 5, 6, 5, 5 },    4, { 1, 2, 1, 3, 5, 4, 1, 2, 1, 1, 4, 5, 2 });
    tester.add_test(max_counters, { 3, 4, 3, 3, 5 }, 5, { 6, 6, 3, 6, 4, 3, 1, 3, 6, 5, 2, 5 });

    tester.run_tests();

    std::cout << tester.test_results() << std::endl;

    return 0;
}
