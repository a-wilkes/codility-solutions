#include <algorithm>
#include <unordered_set>
#include <vector>



int find_earliest_path(const std::size_t river_width, const std::vector<int>& leaves)
{
    std::unordered_set<int> positions_remaining;

    for (std::size_t i = 0; i < leaves.size(); ++i)
    {
        positions_remaining.emplace(leaves.at(i));

        if (positions_remaining.size() == river_width) { return i; }
    }

    return -1;
}


int solution(int X, std::vector<int> &A)
{
    return find_earliest_path(static_cast<std::size_t>(X), A);
}


// main and required includes separated from the above to make copying into Codility cleaner
#include "unit_tester.h"
#include <iostream>

int main()
{
    auto tester = Unit_Tester<int, int, std::vector<int>>();

    tester.add_test(find_earliest_path,  6,  5,  { 1, 3, 1, 4, 2, 3, 5, 4 });
    tester.add_test(find_earliest_path, -1,  2,  { 1, 1, 1, 1 });
    tester.add_test(find_earliest_path,  0,  1,  { 1, 1, 1, 1 });
    tester.add_test(find_earliest_path,  0,  1,  { 1 });
    tester.add_test(find_earliest_path,  18, 10, { 1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
    tester.add_test(find_earliest_path, -1,  5,  { 2, 3, 4, 5 });

    tester.run_tests();

    std::cout << tester.test_results() << std::endl;

    return 0;
}
