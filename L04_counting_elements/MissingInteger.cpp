#include <algorithm>
#include <vector>



int missing_integer(const std::vector<int>& input)
{
    std::vector<int> sorted(input);
    std::sort(begin(sorted), end(sorted));

    int current_min = 1;
    for (const int i : sorted) { if (i == current_min) { current_min += 1; } }

    return current_min;
}


int solution(std::vector<int> &A)
{
    return missing_integer(A);
}



// main and required includes separated from the above to make copying into Codility cleaner
#include "unit_tester.h"
#include <iostream>

int main()
{
    auto tester = Unit_Tester<int, std::vector<int>>();

    tester.add_test(missing_integer, 4, { 1, 2, 3 });
    tester.add_test(missing_integer, 1, { -1, -3 });
    tester.add_test(missing_integer, 1, { -1000000, 1000000 });
    tester.add_test(missing_integer, 4, { 1, 2, 3, 0, 1, 2, 3 });
    tester.add_test(missing_integer, 5, { 1, 3, 6, 4, 1, 2 });

    tester.run_tests();

    std::cout << tester.test_results() << std::endl;

    return 0;
}
