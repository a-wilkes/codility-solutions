#include <algorithm>
#include <vector>



int perm_check(const std::vector<int>& input)
{
    std::vector<int> sorted(input);
    std::sort(begin(sorted), end(sorted));

    int current = 1;
    for (const int i : sorted)
    {
        if (i != current) { return 0; }
        current += 1;
    }

    return 1;
}


int solution(std::vector<int> &A)
{
    return perm_check(A);
}



// main and required includes separated from the above to make copying into Codility cleaner
#include "unit_tester.h"
#include <iostream>

int main()
{
    auto tester = Unit_Tester<int, std::vector<int>>();

    tester.add_test(perm_check, 1, { 4, 1, 3, 2 });
    tester.add_test(perm_check, 0, { 4, 1, 3 });
    tester.add_test(perm_check, 0, { 4, 1, 3, 2, 2 });

    tester.run_tests();

    std::cout << tester.test_results() << std::endl;

    return 0;
}
