CXX = g++
# Note: -Wmissing-declarations removed since Codility doesn't require forward declarations and it makes the cpp files cleaner.
# Note: -Wsign-conversion removed due to flagging too many false positives.
CXXFLAGS = -g -pedantic -Wall -Wextra -Wcast-align -Wcast-qual -Wctor-dtor-privacy -Wdisabled-optimization -Wformat=2 -Winit-self -Wlogical-op -Wmissing-include-dirs -Wnoexcept -Wold-style-cast -Woverloaded-virtual -Wredundant-decls -Wshadow -Wsign-promo -Wstrict-null-sentinel -Wstrict-overflow=5 -Wswitch-default -Wundef -Werror -Wno-unused

OBJ_DIR  := obj
TARGET   := codility
SRC      := $(wildcard *.cpp)

OBJECTS := $(SRC:%.cpp=$(OBJ_DIR)/%.o)

all: build $(TARGET)

$(OBJ_DIR)/%.o: %.cpp
	@mkdir -p $(@D)
	@$(CXX) $(CXXFLAGS) -o $@ -c $<

$(TARGET): $(OBJECTS)
	@mkdir -p $(@D)
	@$(CXX) $(CXXFLAGS) -o $(TARGET) $(OBJECTS)

.PHONY: all build clean

build:
	@mkdir -p $(OBJ_DIR)

clean:
	-@rm $(TARGET)
	-@rm $(OBJ_DIR)/*.o
	-@rmdir $(OBJ_DIR)
