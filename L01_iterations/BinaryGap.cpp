#include <bitset>



std::size_t find_next_one(const int start, const int change, const std::bitset<sizeof(int) * 8> bits)
{
    std::size_t index = start;
    while (index < bits.size() && !bits.test(index)) { index += change; }
    return index;
}


int find_longest_zero_gap(const std::bitset<sizeof(int) * 8> bits)
{
    const std::size_t first_one = find_next_one(0, 1, bits);
    const std::size_t last_one  = find_next_one(bits.size() - 1, -1, bits);

    int longest_gap = 0;
    int current_gap = 0;
    for (std::size_t i = first_one; i <= last_one; ++i)
    {
        const bool found_zero = !bits.test(i);

        longest_gap = found_zero ? longest_gap     : std::max(longest_gap, current_gap);
        current_gap = found_zero ? current_gap + 1 : 0;
    }

    return longest_gap;
}

int solution(int N)
{
    return find_longest_zero_gap(N);
}


// main and required includes separated from the above to make copying into Codility cleaner
#include "unit_tester.h"
#include <iostream>


int main()
{
   
    auto tester = Unit_Tester<int, int>();

    tester.add_test(solution, 2, 0b1001);
    tester.add_test(solution, 0, 0b1);
    tester.add_test(solution, 0, 0b1000);
    tester.add_test(solution, 2, 0b00001001);
    tester.add_test(solution, 2, 0b00100100000);
    tester.add_test(solution, 0, 0b1111);
    tester.add_test(solution, 0, 0b11110);
    tester.add_test(solution, 0, 0b100000);
    tester.add_test(solution, 4, 0b1000010001);

    tester.run_tests();

    std::cout << tester.test_results() << std::endl;

    return 0;
}